package com.qsyout.system;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import org.beetl.core.resource.ClasspathResourceLoader;
import org.beetl.ext.spring.BeetlGroupUtilConfiguration;
import org.beetl.ext.spring.BeetlSpringViewResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import com.qsyout.system.provider.PermissionFunction;

@Configuration
@ComponentScan("com.qsyout")
public class RbAc0AutoConfiguration {

	@Autowired
	Environment env;
	
	@Bean
	public ClasspathResourceLoader createClasspathResourceLoader(){
		ClasspathResourceLoader loader = new ClasspathResourceLoader("views", "UTF-8");
		loader.setAutoCheck("dev".equals(env.getProperty("spring.profiles.active", "prod")));
		return loader;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Bean
    public BeetlGroupUtilConfiguration getBeetlGroupUtilConfiguration(ClasspathResourceLoader loader) {
        BeetlGroupUtilConfiguration config = new BeetlGroupUtilConfiguration();
        config.setResourceLoader(loader);
        config.init();
        config.getGroupTemplate().registerFunction("hasPermission", new PermissionFunction());
        Map settings = new LinkedHashMap<>();
        settings.put("title", env.getProperty("com.qsyout.settings.title"));
        settings.put("headerTitle", env.getProperty("com.qsyout.settings.headerTitle"));
        config.getGroupTemplate().getSharedVars().put("settings", settings);
        return config;
    }

	@Bean
	public BeetlSpringViewResolver createViewResolver(BeetlGroupUtilConfiguration config) throws IOException{
		BeetlSpringViewResolver resolver = new BeetlSpringViewResolver();
		
		resolver.setContentType("text/html;charset=UTF-8");
		resolver.setOrder(0);
		
		resolver.setConfig(config);
		
		return resolver;
	}
}
