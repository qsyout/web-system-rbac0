package com.qsyout.system.consts;

public interface Credentials {

	String ROLE_IDS = "roleIds";
	String USER_ID = "userId";
	String ORG_CODE = "orgCode";
	String USER_NAME = "userName";
	String NICK_NAME = "nickName";
	String ORG_NAME = "orgName";
	String ROLE_NAMES = "roleNames";
	String HEADER_IMG = "headImg";
	String AUTH_PERMISSIONS = "authPermissions";
	String IS_ORIGINAL_PWD = "isOriginalPwd";
	
	String LOGIN_VERIFY_CODE = "loginVerifyCode";
	
	String LAST_LOGIN_DATE = "lastLoginDate";
	
	String LOGIN_URL = "/login.html";
	
	String LOST_PWD_CREDENTIALS = "lostPwdCredentials";
	String LOST_PWD_TIMESTAMP = "lostPwdTimestamp";
}
