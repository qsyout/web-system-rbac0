package com.qsyout.system.provider;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;

import com.qsyout.sdk.declare.BaseException;
import com.qsyout.sdk.declare.ExceptionViewHandler;

@Component
public class MyExceptionViewHandler implements ExceptionViewHandler {

	@Override
	public ModelAndView resolve(HttpServletRequest request, HttpServletResponse response, HandlerMethod handler,
			Exception ex) {
		if(ex instanceof BaseException){
			request.setAttribute("errorStatus", ((BaseException) ex).getStatus());
		}
		
		return new ModelAndView("/500.html");
	}

}
