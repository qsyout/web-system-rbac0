package com.qsyout.system.provider;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.jboss.logging.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.method.HandlerMethod;

import com.qsyout.sdk.annotation.Api;
import com.qsyout.sdk.declare.BaseException;
import com.qsyout.sdk.declare.IbatisDao;
import com.qsyout.sdk.intercept.LogRecord;
import com.qsyout.sdk.util.WebUtil;
import com.qsyout.system.consts.Credentials;

@Service
public class LogRecordService implements LogRecord {

	@Autowired
	IbatisDao ibatisDao;
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void record(HttpServletRequest request, HandlerMethod handler, Exception ex, long use) {
		Api api = handler.getMethodAnnotation(Api.class);
		if(api == null||!api.record()){
			return ;
		}
		Map params = new HashMap<>(); 
		params.put("sn", MDC.get("sn"));
		params.put("use", use);
		
		int status = 1200;
		if(ex instanceof BaseException){
			status = ((BaseException) ex).getStatus();
			params.put("ex", ex.getMessage());
		}else if(ex instanceof Exception){
			status = 2500;
			params.put("ex", "系统错误");
		}
		
		params.put("status", status);
		params.put("userId", request.getSession().getAttribute(Credentials.USER_ID));
		params.put("open", api == null?1:(api.open()?1:2));
		params.put("desc", api == null?null:api.desc());
		params.put("url", WebUtil.getPathWithinApplication(request));
		
		new Thread(() -> {
			ibatisDao.update("SYS_LOG.insert", params);
		}).start();
	}

}
