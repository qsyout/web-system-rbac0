package com.qsyout.system.provider;

import java.util.List;

import org.beetl.core.Context;
import org.beetl.core.Function;
import org.springframework.util.CollectionUtils;

import com.qsyout.sdk.util.SubjectUtil;
import com.qsyout.system.consts.Credentials;

public class PermissionFunction implements Function {

	@SuppressWarnings("unchecked")
	@Override
	public Object call(Object[] paras, Context ctx) {
		try {
			if(SubjectUtil.isDevMode()||Long.parseLong(SubjectUtil.getSession(Credentials.USER_ID) + "") == 1){
				return true;
			}
		} catch (Exception e) {
		}
		
		List<String> permissions = (List<String>) SubjectUtil.getSession(Credentials.AUTH_PERMISSIONS);
		
		if(CollectionUtils.isEmpty(permissions)||paras == null||paras.length == 0){
			return false;
		}
		
		for (Object para : paras) {
			if(permissions.contains(para)){
				return true;
			}
		}
		
		return false;
	}

}
