package com.qsyout.system.provider;

import java.util.List;

import javax.servlet.http.Cookie;

import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.qsyout.sdk.declare.AuthInterceptor;
import com.qsyout.sdk.util.SubjectUtil;
import com.qsyout.system.consts.Credentials;

@Service
public class Rbac0AuthInterceptor implements AuthInterceptor {
	
	@Override
	public boolean login(Cookie[] cookies) {
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean handle(String permission) {
		if((int)SubjectUtil.getSession(Credentials.USER_ID) == 1){
			return true;
		}
		List<String> caches = (List<String>) SubjectUtil.getSession(Credentials.AUTH_PERMISSIONS);
		if(!CollectionUtils.isEmpty(caches)){
			return caches.contains(permission);
		}
		return false;
	}

	@Override
	public void setDevPerssion() {
		if(!SubjectUtil.isLogin()){
			SubjectUtil.setSession(Credentials.ROLE_IDS, "1");
			SubjectUtil.setSession(Credentials.USER_ID, 1);
			SubjectUtil.setSession(Credentials.ORG_CODE, "0001");
		}
	}

	@Override
	public String loginUrl() {
		return Credentials.LOGIN_URL;
	}

}
