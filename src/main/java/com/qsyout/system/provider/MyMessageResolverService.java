package com.qsyout.system.provider;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qsyout.sdk.declare.IbatisDao;
import com.qsyout.sdk.declare.MyMessageResolver;

@Service
public class MyMessageResolverService implements MyMessageResolver {

	private Map<String, String> cache = new HashMap<String, String>();
	
	@Autowired
	IbatisDao ibatisDao;
	
	@Override
	public String resolve(String key, String message) {
		if(!cache.containsKey(key)){
			synchronized (cache) {
				if(!cache.containsKey(key)){
					String value = ibatisDao.load("SYS_MESSAGE.getByKey", key);
					if(value == null){
						cache.put(key, message);
					}else{
						cache.put(key, value);
					}
				}
			}
		}
		
		return cache.get(key);
	}
	
	public synchronized void update(String key){
		cache.remove(key);
	}

}
