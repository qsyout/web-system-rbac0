package com.qsyout.system.json.personal;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.qsyout.sdk.annotation.Api;
import com.qsyout.sdk.declare.BaseService;
import com.qsyout.sdk.util.SubjectUtil;
import com.qsyout.system.consts.Credentials;

@Service
public class GetMyUserInfoService extends BaseService {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	@Api(desc="个人中心->个人资料",record=false)
	public Object execute(Map params) {
		params.put("id", SubjectUtil.getSession(Credentials.USER_ID));
		return ibatisDao.load("SYS_USER.getMyUserInfo", params);
	}

}
