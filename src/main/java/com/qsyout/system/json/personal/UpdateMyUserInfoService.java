package com.qsyout.system.json.personal;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.qsyout.sdk.annotation.Api;
import com.qsyout.sdk.annotation.JsonResult;
import com.qsyout.sdk.declare.BaseService;
import com.qsyout.sdk.ex.ForbiddenException;
import com.qsyout.sdk.util.SubjectUtil;
import com.qsyout.system.consts.Credentials;

@Service
public class UpdateMyUserInfoService extends BaseService {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	@Api(desc="个人中心->个人资料->修改个人资料")
	@JsonResult(info=true)
	public Object execute(Map params) {
		if((long)params.get("id") !=  Long.parseLong(SubjectUtil.getSession(Credentials.USER_ID) + "")){
			throw new ForbiddenException("UpdateMyUserInfoService.01");
		}
		
		params.put("updateId", SubjectUtil.getSession(Credentials.USER_ID));
		
		ibatisDao.update("SYS_USER.updateMyUserInfo", params);
		return null;
	}

}
