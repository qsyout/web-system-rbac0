package com.qsyout.system.json.database;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qsyout.sdk.annotation.Api;
import com.qsyout.sdk.declare.BaseService;

@Service
public class KillExecutingSqlService extends BaseService {

	@Autowired
	DataSource dataSource;
	
	@SuppressWarnings("rawtypes")
	@Override
	@Api(desc="数据库工具->杀掉数据进程",permission="system:database:kill")
	public Object execute(Map params) {
		Statement statement = null;
		Connection conn = null;
		try {
			conn = dataSource.getConnection();
			statement = conn.createStatement();
			
			statement.execute(String.format("kill %s", params.get("id")));
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if(statement != null){
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(conn != null){
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

}
