package com.qsyout.system.json.database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qsyout.sdk.annotation.Api;
import com.qsyout.sdk.declare.BaseService;

@Service
public class ExecuteQuerySqlService extends BaseService {

	@Autowired
	DataSource dataSource;
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	@Api(desc="执行sql(DQL)")
	public Object execute(Map params) {
		Map result = new HashMap(3);
		Statement statement = null;
		Connection conn = null;
		try {
			result.put("status", 200);
			conn = dataSource.getConnection();
			statement = conn.createStatement();

			ResultSet set = statement.executeQuery((String) params.get("sql"));
			ResultSetMetaData metaData = set.getMetaData();
			
			String[] columns = new String[metaData.getColumnCount()];
			List<Object[]> datas = new LinkedList<Object[]>();
			while(set.next()){
				Object[] data = new Object[columns.length];
				for (int i = 0; i < columns.length; i++) {
					if (columns[i] == null) {
						columns[i] = metaData.getColumnLabel(i + 1);
					}
					data[i] = set.getObject(columns[i]);
				}
				datas.add(data);
			}
			
			result.put("columns", columns);
			result.put("datas", datas);
			result.put("message", String.format("执行成功,查询结果%s", datas.size()));
			
			return result;
		} catch (SQLException e) {
			e.printStackTrace();
			result.put("status", 500);
			result.put("message", e.getMessage());
			return result;
		} finally {
			if(statement != null){
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(conn != null){
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
