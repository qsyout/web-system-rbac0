package com.qsyout.system.json.database;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.qsyout.sdk.declare.BaseService;

@Service
public class LoadExecutingSqlService extends BaseService {

	@SuppressWarnings("rawtypes")
	@Override
	public Object execute(Map params) {
		return ibatisDao.queryForList("DATABASE_UTIL.loadExecutingSql", params);
	}

}
