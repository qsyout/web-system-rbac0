package com.qsyout.system.json.database;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qsyout.sdk.annotation.Api;
import com.qsyout.sdk.declare.BaseService;

@Service
public class ExecuteUpdateSqlService extends BaseService {

	@Autowired
	DataSource dataSource;
	
	@SuppressWarnings("rawtypes")
	@Override
	@Api(desc="执行sql(DDL/DML/DCL)")
	public Object execute(Map params) {
		Statement statement = null;
		Connection conn = null;
		try {
			conn = dataSource.getConnection();
			statement = conn.createStatement();
			
			return String.format("执行成功:受影响的行%s", statement.executeUpdate((String) params.get("sql")));
		} catch (SQLException e) {
			e.printStackTrace();
			return e.getMessage();
		} finally {
			if(statement != null){
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(conn != null){
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
