package com.qsyout.system.json.log;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.qsyout.sdk.annotation.Api;
import com.qsyout.sdk.declare.BaseService;

@Service
public class ShowLogDetailService extends BaseService {
	
	@Autowired
	Environment env;
	
	private final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
	private final String startLine = "**************************START**************************";
	private final String endLine = "**************************END**************************";

	@SuppressWarnings("rawtypes")
	@Override
	@Api(desc="系统管理->日志管理",record=false,permission="system:log")
	public Object execute(Map params) {
		if(env.getProperty("com.qsyout.sdk.log.path") == null){
			return null;
		}
		
		Map log = ibatisDao.load("SYS_LOG.getById", params);
		String url = (String) log.get("url");
		
		StringBuilder path = new StringBuilder(env.getProperty("com.qsyout.sdk.log.path"));
		path.append("/");
		path.append(sdf.format(log.get("createDate")));
		path.append("/");
		if (url.lastIndexOf("/") > 0) {
			path.append(url.substring(0, url.lastIndexOf("/")));
		}
		File parent = new File(path.toString());
		if(!parent.exists()){
			return null;
		}
		
		File[] dirs = parent.listFiles();
		if(dirs != null){
			for (File file : dirs) {
				if (file.getName().contains(
						url.substring(url.lastIndexOf("/") + 1,url.lastIndexOf(".")))) {
					
					List<String> contents = search(file, (String) log.get("sn"));
					
					if (contents != null) {
						return contents;
					}
				}
			}
		}
		
		return null;
	}
	
	private List<String> search(File logFile, String sn) {

		List<String> contents = null;

		logger.info("搜索日志:" + logFile.getAbsolutePath());
		
		Scanner scan = null;
		try {
			scan = new Scanner(logFile,"utf-8");

			boolean flag = false;

			while (scan.hasNext()) {
				
				String line = scan.nextLine();
				if (line.contains(sn) && line.contains(startLine)) {
					contents = new LinkedList<String>();
					flag = true;
				}

				if (flag) {
					contents.add(line);
				}

				if (line.contains(sn) && line.contains(endLine)) {
					break;
				}
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}finally {
			if(scan != null){
				scan.close();
			}
		}

		return contents;
	}

}
