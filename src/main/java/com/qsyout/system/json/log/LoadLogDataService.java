package com.qsyout.system.json.log;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.qsyout.sdk.annotation.Api;
import com.qsyout.sdk.declare.BaseService;

@Service
public class LoadLogDataService extends BaseService {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	@Api(desc="系统管理->日志管理",record=false,permission="system:log")
	public Object execute(Map params) {
		long pageNumber = (long) params.get("pageNumber");
		long pageSize = (long) params.get("pageSize");
		
		params.put("start", (pageNumber - 1)*pageSize);
		
		Map result = new HashMap(2);
		result.put("total", ibatisDao.load("SYS_LOG.pageQueryCount", params));
		result.put("list", ibatisDao.queryForList("SYS_LOG.pageQuery", params));
		
		return result;
	}

}
