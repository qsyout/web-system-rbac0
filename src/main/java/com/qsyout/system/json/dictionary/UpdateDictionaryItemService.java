package com.qsyout.system.json.dictionary;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.qsyout.sdk.annotation.Api;
import com.qsyout.sdk.annotation.JsonResult;
import com.qsyout.sdk.declare.BaseService;
import com.qsyout.sdk.ex.BusinessException;
import com.qsyout.sdk.util.SubjectUtil;
import com.qsyout.system.consts.Credentials;
import com.qsyout.validate.ex.ParameterValidateException;

@Service
public class UpdateDictionaryItemService extends BaseService {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	@Api(desc="系统管理->数据字典->更新数据字典名称",record=false,permission="system:dictionary:update")
	@JsonResult(info=true)
	public Object execute(Map params) {
		if("dictionaryName".equals(params.get("field"))&&(params.get("value") == null||"".equals(params.get("value")))){
			throw new ParameterValidateException("字典名称不能为空");
		}
		if((int)ibatisDao.load("SYS_DICTIONARY.existSameDictionaryName", params) > 0){
			throw new BusinessException("UpdateDictionaryItemService.01", "同级别字典存在相同的字典名称");
		}
		
		params.put("updateId", SubjectUtil.getSession(Credentials.USER_ID));
		
		ibatisDao.update("SYS_DICTIONARY.update", params);
		return null;
	}

}
