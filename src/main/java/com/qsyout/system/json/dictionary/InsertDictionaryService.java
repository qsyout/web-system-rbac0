package com.qsyout.system.json.dictionary;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.qsyout.sdk.annotation.Api;
import com.qsyout.sdk.annotation.JsonResult;
import com.qsyout.sdk.declare.BaseService;
import com.qsyout.sdk.ex.BusinessException;
import com.qsyout.sdk.util.SubjectUtil;
import com.qsyout.system.consts.Credentials;

@Service
public class InsertDictionaryService extends BaseService {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	@Api(desc="系统管理->数据字典->新增数据字典",record=false,permission="system:dictionary:insert")
	@JsonResult(info=true)
	public Object execute(Map params) {
		//校验数据字典组名称
		if((long)params.get("pid") == 0&&(int)ibatisDao.load("SYS_DICTIONARY.existSameGroupName", params) > 0){
			throw new BusinessException("InsertDictionaryService.01", String.format("字典组[%s]中存在相同的字典名称", params.get("groupName")));
		}else if((long)params.get("pid") > 0&&(int)ibatisDao.load("SYS_DICTIONARY.existSameDictionaryName", params) > 0){
			throw new BusinessException("InsertDictionaryService.02", "同级别字典存在相同的字典名称");
		}
		
		params.put("dictionaryValue", ibatisDao.load("SYS_DICTIONARY.loadNextValue", params));
		
		params.put("createId", SubjectUtil.getSession(Credentials.USER_ID));
		
		ibatisDao.update("SYS_DICTIONARY.insert", params);
		return null;
	}

}
