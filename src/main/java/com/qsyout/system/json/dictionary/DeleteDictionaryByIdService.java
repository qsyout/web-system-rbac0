package com.qsyout.system.json.dictionary;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.qsyout.sdk.annotation.Api;
import com.qsyout.sdk.annotation.JsonResult;
import com.qsyout.sdk.declare.BaseService;
import com.qsyout.sdk.ex.BusinessException;
import com.qsyout.sdk.util.SubjectUtil;
import com.qsyout.system.consts.Credentials;

@Service
public class DeleteDictionaryByIdService extends BaseService {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	@Api(desc="系统管理->数据字典->删除数据字典",record=false,permission="system:dictionary:delete")
	@JsonResult(info=true)
	public Object execute(Map params) {
		// 校验没有子级
		if ((int) ibatisDao.load("SYS_DICTIONARY.getChildrenCount", params) > 0) {
			throw new BusinessException("DeleteDictionaryByIdService.01", "含有子级数据字典项!");
		}

		params.put("deleteId", SubjectUtil.getSession(Credentials.USER_ID));

		ibatisDao.update("SYS_DICTIONARY.deleteDictionaryById", params);
		return null;
	}

}
