package com.qsyout.system.json.dictionary;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.qsyout.sdk.annotation.Api;
import com.qsyout.sdk.declare.BaseService;

@Service
public class LoadDictionaryDataService extends BaseService {

	@SuppressWarnings("rawtypes")
	@Override
	@Api(desc="系统管理->数据字典",record=false,permission="system:dictionary")
	public Object execute(Map params) {
		return ibatisDao.queryForList("SYS_DICTIONARY.loadDictionaryData", params);
	}

}
