package com.qsyout.system.json.role;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.qsyout.sdk.annotation.Api;
import com.qsyout.sdk.declare.BaseService;

@Service
public class GetAllMenuListService extends BaseService {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	@Api(desc="系统管理->角色管理->加载所有菜单数据 ",record=false)
	public Object execute(Map params) {
		List<Map> list = ibatisDao.queryForList("SYS_MENU.queryViewMenuList", params);
		if(!CollectionUtils.isEmpty(list)){
			for (Map map : list) {
				map.put("pid", map.get("id"));
				map.put("children", ibatisDao.queryForList("SYS_MENU.queryOperateMenuList", map));
			}
		}
		return list;
	}

}
