package com.qsyout.system.json.role;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.qsyout.sdk.annotation.Api;
import com.qsyout.sdk.declare.BaseService;

@Service
public class LoadAllRoleDataService extends BaseService {

	@SuppressWarnings("rawtypes")
	@Override
	@Api(desc="系统管理->角色管理->加载所有角色数据 ",record=false)
	public Object execute(Map params) {
		return ibatisDao.queryForList("SYS_ROLE.loadAllRoleData", params);
	}

}
