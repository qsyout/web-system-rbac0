package com.qsyout.system.json.role;

import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.qsyout.sdk.annotation.Api;
import com.qsyout.sdk.annotation.JsonResult;
import com.qsyout.sdk.declare.BaseService;
import com.qsyout.sdk.ex.BusinessException;
import com.qsyout.sdk.util.SubjectUtil;
import com.qsyout.system.consts.Credentials;

@Service
public class InsertRoleService extends BaseService {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	@Transactional
	@Api(desc="系统管理->角色管理->新增角色",permission="system:role:insertMenu")
	@JsonResult(info=true)
	public Object execute(Map params) {
		//角色名称验证，不得重复
		if((int)ibatisDao.load("SYS_ROLE.existRoleName", params) > 0){
			throw new BusinessException("InsertRoleService.01", "角色已存在，请勿重复添加");
		}
		params.put("createId", SubjectUtil.getSession(Credentials.USER_ID));
		ibatisDao.update("SYS_ROLE.insert", params);
		return null;
	}

}
