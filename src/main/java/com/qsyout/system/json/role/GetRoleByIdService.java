package com.qsyout.system.json.role;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.qsyout.sdk.annotation.Api;
import com.qsyout.sdk.declare.BaseService;

@Service
public class GetRoleByIdService extends BaseService {

	@SuppressWarnings("rawtypes")
	@Override
	@Api(desc="系统管理->角色管理->根据id加载角色信息",record=false)
	public Object execute(Map params) {
		return ibatisDao.load("SYS_ROLE.getRoleInfoById", params);
	}

}
