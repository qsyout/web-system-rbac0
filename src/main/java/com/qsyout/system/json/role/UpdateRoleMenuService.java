package com.qsyout.system.json.role;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.qsyout.sdk.annotation.Api;
import com.qsyout.sdk.annotation.JsonResult;
import com.qsyout.sdk.declare.BaseService;
import com.qsyout.sdk.util.SubjectUtil;
import com.qsyout.system.consts.Credentials;

@Service
public class UpdateRoleMenuService extends BaseService {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	@Api(desc="系统管理->角色管理->权限设置->更新权限",permission="system:role:roleMenu")
	@Transactional
	@JsonResult(info=true)
	public Object execute(Map params) {
		//清除角色对应的权限
		ibatisDao.update("SYS_ROLE_MENU.deleteByRoleId", params);
		if (CollectionUtils.isEmpty((List)params.get("params"))) {
			ibatisDao.update("SYS_ROLE_MENU.batchInsert", params);
			ibatisDao.update("SYS_ROLE_MENU.batchInsertTopMenu", params);
		}
		params.put("updateId", SubjectUtil.getSession(Credentials.USER_ID));
		params.put("id", params.get("roleId"));
		ibatisDao.update("SYS_ROLE.updatePopedoms", params);
		return null;
	}

}
