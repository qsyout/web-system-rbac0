package com.qsyout.system.json.role;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.qsyout.sdk.annotation.Api;
import com.qsyout.sdk.declare.BaseService;

@Service
public class LoadRoleMenuDataService extends BaseService {

	@SuppressWarnings("rawtypes")
	@Override
	@Api(desc="系统管理->角色管理->根据角色id加载相关联的配置项",record=false)
	public Object execute(Map params) {
		return ibatisDao.queryForList("SYS_ROLE_MENU.queryByRoleId", params);
	}

}
