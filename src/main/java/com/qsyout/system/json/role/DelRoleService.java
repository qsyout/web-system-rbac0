package com.qsyout.system.json.role;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.qsyout.sdk.annotation.Api;
import com.qsyout.sdk.annotation.JsonResult;
import com.qsyout.sdk.declare.BaseService;
import com.qsyout.sdk.ex.BusinessException;
import com.qsyout.sdk.util.SubjectUtil;
import com.qsyout.system.consts.Credentials;

@Service
public class DelRoleService extends BaseService {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	@Api(desc="系统管理->角色管理->删除角色",permission="system:role:delMenu")
	@JsonResult(info=true)
	public Object execute(Map params) {
		//校验角色下是否有人
		params.put("roleId", params.get("id"));
		if((int)ibatisDao.load("SYS_USER_ROLE.existUser", params) > 0){
			throw new BusinessException("DelRoleService.01", "该角色关联有用户，无法删除");
		}
		Map role = ibatisDao.load("SYS_ROLE.getById", params);
		if(role == null||(int)role.get("isSystem") == 1){
			throw new BusinessException("DelRoleService.02", "该角色不存在或者该角色为系统预留项无法删除");
		}
		params.put("deleteId", SubjectUtil.getSession(Credentials.USER_ID));
		ibatisDao.update("SYS_ROLE.delete", params);
		return null;
	}

}
