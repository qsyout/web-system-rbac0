package com.qsyout.system.json.role;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.qsyout.sdk.annotation.Api;
import com.qsyout.sdk.declare.BaseService;

@Service
public class LoadRoleDataService extends BaseService {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	@Api(desc="系统管理->角色管理->加载角色列表数据",record=false,permission="system:role")
	public Object execute(Map params) {
		long pageNumber = (long) params.get("pageNumber");
		long pageSize = (long) params.get("pageSize");
		
		params.put("start", (pageNumber - 1)*pageSize);
		
		Map result = new HashMap(2);
		result.put("total", ibatisDao.load("SYS_ROLE.pageQueryCount", params));
		result.put("list", ibatisDao.queryForList("SYS_ROLE.pageQuery", params));
		
		return result;
	}

}
