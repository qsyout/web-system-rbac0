package com.qsyout.system.json.safe;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.qsyout.sdk.annotation.Api;
import com.qsyout.sdk.declare.BaseService;

@Service
public class GetSafeQuestionOptionsService extends BaseService {

	@SuppressWarnings({ "rawtypes" })
	@Override
	@Api(desc="个人中心->安全中心",record=false,open=true)
	public Object execute(Map params) {
		return ibatisDao.queryForList("SYS_SAFE_ANSWERS.getSafeQuestionOptions", params);
	}

}
