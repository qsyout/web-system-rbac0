package com.qsyout.system.json.safe;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.qsyout.sdk.annotation.Api;
import com.qsyout.sdk.annotation.JsonResult;
import com.qsyout.sdk.declare.BaseService;
import com.qsyout.sdk.ex.BusinessException;
import com.qsyout.sdk.util.EncryptUtil;
import com.qsyout.sdk.util.SubjectUtil;
import com.qsyout.system.consts.Credentials;

@Service
public class UpdateMyLoginPwdService extends BaseService {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	@Api(desc="个人中心->安全中心->修改密码")
	@JsonResult(info=true)
	public Object execute(Map params) {
		if(params.get("oldLoginPwd").toString().equals(params.get("newLoginPwd"))){
			throw new BusinessException("UpdateMyLoginPwdService.01", "新旧密码不能一致");
		}
		if(params.get("newLoginPwd").toString().equals(params.get("doubleLoginPwd"))){
			throw new BusinessException("UpdateMyLoginPwdService.02", "两次密码输入不一致");
		}
		
		params.put("id", SubjectUtil.getSession(Credentials.USER_ID));
		
		Map user = ibatisDao.load("SYS_USER.getById", params);
		
		if(!user.get("loginPwd").toString().equals(EncryptUtil.encrypt(params.get("oldLoginPwd").toString(), (String) user.get("salt")))){
			throw new BusinessException("UpdateMyLoginPwdService.03", "原密码输入错误");
		}
		
		params.put("loginPwd", EncryptUtil.encrypt((String) params.get("newLoginPwd"), (String) user.get("salt")));
		ibatisDao.update("SYS_USER", params);
		
		SubjectUtil.removeSession(Credentials.IS_ORIGINAL_PWD);
		
		return null;
	}

}
