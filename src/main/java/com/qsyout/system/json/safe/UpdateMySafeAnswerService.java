package com.qsyout.system.json.safe;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.qsyout.sdk.annotation.Api;
import com.qsyout.sdk.annotation.JsonResult;
import com.qsyout.sdk.declare.BaseService;
import com.qsyout.sdk.util.SubjectUtil;
import com.qsyout.system.consts.Credentials;

@Service
public class UpdateMySafeAnswerService extends BaseService {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	@Api(desc="个人中心->安全中心->密保问题更新")
	@JsonResult(info=true)
	public Object execute(Map params) {
		//删除关联的所有密保
		params.put("userId", SubjectUtil.getSession(Credentials.USER_ID));
		
		ibatisDao.update("SYS_SAFE_ANSWERS.deleteAllAnswers", params);
		
		List<Map> list = (List<Map>) params.get("list");
		for (Map map : list) {
			if(map.get("id") != null&&(int)ibatisDao.load("SYS_SAFE_ANSWERS.isUpdateQuestionId", params) > 0){
				ibatisDao.update("SYS_SAFE_ANSWERS.updateAnswerById", map);
			}else{
				map.put("userId", SubjectUtil.getSession(Credentials.USER_ID));
				ibatisDao.update("SYS_SAFE_ANSWERS.insert", map);
			}
		}
		return null;
	}

}
