package com.qsyout.system.json.safe;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.qsyout.sdk.annotation.Api;
import com.qsyout.sdk.declare.BaseService;
import com.qsyout.sdk.ex.BusinessException;
import com.qsyout.sdk.util.EncryptUtil;
import com.qsyout.sdk.util.SubjectUtil;
import com.qsyout.system.consts.Credentials;

@Service
public class ResetMyLoginPwdService extends BaseService {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	@Api(desc="重置密码",open=true)
	public Object execute(Map params) {
		SubjectUtil.removeSession(Credentials.LOST_PWD_TIMESTAMP);
		
		//验证2次新密码
		if(!((Map)params.get("pwdModel")).get("newLoginPwd").toString().equals(((Map)params.get("pwdModel")).get("doubleLoginPwd"))){
			throw new BusinessException("ResetMyLoginPwdService.01", "两次密码输入不一致");
		}
		
		if("question".equals(params.get("type"))){
			String code = (String) SubjectUtil.getSession(Credentials.LOGIN_VERIFY_CODE);
			SubjectUtil.removeSession(Credentials.LOGIN_VERIFY_CODE);
			if(!((String)((Map)params.get("question")).get("verifyCode")).equals(code)){
				throw new BusinessException("ResetMyLoginPwdService.02", "验证码输入错误");
			}
			//验证密保问题
			params.put("questionId", ((Map)params.get("question")).get("id"));
			String answer = ibatisDao.load("SYS_SAFE_ANSWERS.getAnswerByLoginNameAndQid", params);
			if(answer == null||!answer.equals(((Map)params.get("question")).get("answer"))){
				return false;
			}
		}else if("email".equals(params.get("type"))){
			String credentials = (String) SubjectUtil.getSession(Credentials.LOST_PWD_CREDENTIALS);
			SubjectUtil.removeSession(Credentials.LOST_PWD_CREDENTIALS);
			if(!EncryptUtil.encrypt((String)params.get("loginName"), (String)((Map)params.get("email")).get("verifyCode")).equals(credentials)){
				return false;
			}
		}
		
		try {
			Map user = ibatisDao.load("SYS_USER.getByLoginName", params);
			//更新密码
			params.put("loginPwd", EncryptUtil.encrypt((String)((Map)params.get("pwdModel")).get("newLoginPwd"), (String) user.get("salt")));
			ibatisDao.update("SYS_USER.updateMyPwdInfoByLoginName", params);
			SubjectUtil.removeSession(Credentials.IS_ORIGINAL_PWD);
			
			return true;
		} catch (Exception e) {
		}
		
		return false;
	}

}
