package com.qsyout.system.json.safe;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.qsyout.sdk.annotation.Api;
import com.qsyout.sdk.declare.BaseService;
import com.qsyout.sdk.util.SubjectUtil;
import com.qsyout.system.consts.Credentials;

@Service
public class GetMySafeAnswersService extends BaseService {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	@Api(desc="个人中心->安全中心",record=false)
	public Object execute(Map params) {
		params.put("userId", SubjectUtil.getSession(Credentials.USER_ID));
		return ibatisDao.queryForList("SYS_SAFE_ANSWERS.getMySafeAnswers", params);
	}

}
