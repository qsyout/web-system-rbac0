package com.qsyout.system.json.safe;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.qsyout.sdk.annotation.Api;
import com.qsyout.sdk.declare.BaseService;
import com.qsyout.sdk.ex.BusinessException;
import com.qsyout.sdk.util.EncryptUtil;
import com.qsyout.sdk.util.MailUtil;
import com.qsyout.sdk.util.StringUtil;
import com.qsyout.sdk.util.SubjectUtil;
import com.qsyout.system.consts.Credentials;

@Service
public class LostPwd2SendMailService extends BaseService {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	@Api(desc="找回密码发送邮件服务",open=true)
	public Object execute(Map params) {
		Long timestamp = (Long) SubjectUtil.getSession(Credentials.LOST_PWD_TIMESTAMP);
		if(timestamp != null&&timestamp/1000/60 < 2){
			throw new BusinessException("LostPwd2SendMailService.01", "请勿频繁发送验证码");
		}
		
		//根据登录账号查询邮箱账号
		String email = ibatisDao.load("SYS_USER.getEmailByLoginName", params);
		if(email != null){
			//找回密码验证的模板编号  mail_templates   1
			params.put("groupName", "mail_templates");
			params.put("dictionaryValue", 1);
			String template = ibatisDao.load("SYS_DICTIONARY.loadDictionaryDescByGroupAndValue", params);
			if(template != null){
				String random = StringUtil.random(6);
				if(MailUtil.send(email, "找回密码", String.format(template, params.get("loginName"),random,SubjectUtil.getRequest().getServerName()))){
					SubjectUtil.setSession(Credentials.LOST_PWD_CREDENTIALS, EncryptUtil.encrypt((String) params.get("loginName"), random));
					SubjectUtil.setSession(Credentials.LOST_PWD_TIMESTAMP, System.currentTimeMillis());
				}
			}
		}
		
		return null;
	}

}
