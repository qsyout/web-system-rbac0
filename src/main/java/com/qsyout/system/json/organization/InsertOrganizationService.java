package com.qsyout.system.json.organization;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.qsyout.sdk.annotation.Api;
import com.qsyout.sdk.annotation.JsonResult;
import com.qsyout.sdk.declare.BaseService;
import com.qsyout.sdk.ex.BusinessException;
import com.qsyout.sdk.util.SubjectUtil;
import com.qsyout.system.consts.Credentials;

/**  
 * @Description:  新增组织机构 
 * @author: dangsj     
 * @date: 2020-08-08 11:08 
 * @Copyright: All rights Reserved, Designed By qsyout.com
 */
@Service
public class InsertOrganizationService extends BaseService {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	@Api(desc="系统管理->组织管理->新增组织机构 ",permission="organization:insert")
	@JsonResult(info=true)
	public Object execute(Map params) {
		//同级别组织名称不能一样
		if((int)ibatisDao.load("SYS_ORGANIZATION.existSameName", params) > 0){
			throw new BusinessException("InsertOrganizationService.01", "当前级别存在相同的组织名称!");
		}
		
		params.put("code", ibatisDao.load("SYS_ORGANIZATION.loadNextOrganizationCodeByPid", params));
		params.put("createId", SubjectUtil.getSession(Credentials.USER_ID));
		
		ibatisDao.update("SYS_ORGANIZATION.insert", params);
		return null;
	}

}
