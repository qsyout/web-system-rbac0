package com.qsyout.system.json.organization;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.qsyout.sdk.annotation.Api;
import com.qsyout.sdk.annotation.JsonResult;
import com.qsyout.sdk.declare.BaseService;
import com.qsyout.sdk.ex.BusinessException;
import com.qsyout.sdk.util.SubjectUtil;
import com.qsyout.system.consts.Credentials;

/**  
 * @Description:删除组织机构
 * @author: dangsj     
 * @date: 2020-08-08 11:12 
 * @Copyright: All rights Reserved, Designed By qsyout.com
 */
@Service
public class DelOrganizationService extends BaseService {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	@Api(desc="系统管理->组织管理->删除组织机构 ",permission="organization:delete")
	@JsonResult(info=true)
	public Object execute(Map params) {
		//判断当前组织下是否有子级组织机构
		if((int)ibatisDao.load("SYS_ORGANIZATION.hasChildren", params) > 0){
			throw new BusinessException("DelOrganizationService.01", "存在下级组织机构，无法删除");
		}
		//判断当前组织下是否有用户存在
		params.put("orgId", params.get("id"));
		if((int)ibatisDao.load("SYS_USER.existUser", params) > 0){
			throw new BusinessException("DelOrganizationService.02", "当前级别存在相同的组织名称!");
		}
		
		params.put("deleteId", SubjectUtil.getSession(Credentials.USER_ID));
		
		ibatisDao.update("SYS_ORGANIZATION.delete", params);
		return null;
	}

}
