package com.qsyout.system.json.organization;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.qsyout.sdk.annotation.Api;
import com.qsyout.sdk.declare.BaseService;
import com.qsyout.sdk.util.SubjectUtil;
import com.qsyout.system.consts.Credentials;

/**  
 * @Description: 加载组织机构树形数据   
 * @author: dangsj     
 * @date: 2020-08-07 16:10 
 * @Copyright: All rights Reserved, Designed By qsyout.com
 */
@Service
@SuppressWarnings({ "rawtypes", "unchecked" })
public class LoadOrganizationTreeDataService extends BaseService {

	@Override
	@Api(desc="系统管理->组织管理->加载组织机构树形数据 ",record=false)
	public Object execute(Map params) {
		params.put("code", SubjectUtil.getSession(Credentials.ORG_CODE));
		return treeData(ibatisDao.queryForList("SYS_ORGANIZATION.loadOrganizationTreeData", params));
	}
	
	private List<Map> treeData(List<Map> original){
		List<Map> treeData = new LinkedList<Map>();
		if(!CollectionUtils.isEmpty(original)){
			for (Map map : original){
				if(((String) map.get("code")).length() == 4){
					Map parent = new LinkedHashMap();
					treeData.add(parent);
					parent.putAll(map);
					children(parent, original);
				}
			}
		}
		return treeData;
	}
	
	private void children(Map parent,List<Map> original){
		for (Map map : original) {
			if(((String) map.get("code")).startsWith((String) parent.get("code"))
					&&((String) map.get("code")).length() > ((String) parent.get("code")).length()){
				List<Map> children = (List<Map>) parent.get("children");
				if(children == null){
					children = new LinkedList<Map>();
					parent.put("children", children);
				}
				children.add(map);
				children(map, original);
			}else if(!((String) map.get("code")).startsWith((String) parent.get("code"))&&parent.get("children") != null){
				break;
			}
		}
	}
}
