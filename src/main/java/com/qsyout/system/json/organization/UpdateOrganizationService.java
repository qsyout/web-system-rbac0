package com.qsyout.system.json.organization;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.qsyout.sdk.annotation.Api;
import com.qsyout.sdk.annotation.JsonResult;
import com.qsyout.sdk.declare.BaseService;
import com.qsyout.sdk.ex.BusinessException;
import com.qsyout.sdk.util.SubjectUtil;
import com.qsyout.system.consts.Credentials;

/**
 * @Description:更新组织机构名称
 * @author: dangsj
 * @date: 2020-08-08 11:12
 * @Copyright: All rights Reserved, Designed By qsyout.com
 */
@Service
public class UpdateOrganizationService extends BaseService {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	@Api(desc="系统管理->组织管理->更新组织机构名称",permission="organization:update")
	@JsonResult(info=true)
	public Object execute(Map params) {
		if ((int) ibatisDao.load("SYS_ORGANIZATION.existSameName", params) > 0) {
			throw new BusinessException("UpdateOrganizationService.01", "当前级别存在相同的组织名称!");
		}

		params.put("updateId", SubjectUtil.getSession(Credentials.USER_ID));

		ibatisDao.update("SYS_ORGANIZATION.update", params);
		return null;
	}

}
