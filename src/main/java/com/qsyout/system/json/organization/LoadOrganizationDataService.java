package com.qsyout.system.json.organization;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.qsyout.sdk.annotation.Api;
import com.qsyout.sdk.declare.BaseService;
import com.qsyout.sdk.util.SubjectUtil;
import com.qsyout.system.consts.Credentials;

/**  
 * @Description: 加载组织机构数据   
 * @author: dangsj     
 * @date: 2020-08-07 16:10 
 * @Copyright: All rights Reserved, Designed By qsyout.com
 */
@Service
public class LoadOrganizationDataService extends BaseService {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	@Api(desc="系统管理->组织管理->加载组织机构数据 ",record=false)
	public Object execute(Map params) {
		params.put("code", SubjectUtil.getSession(Credentials.ORG_CODE));
		return ibatisDao.queryForList("SYS_ORGANIZATION.loadOrganizationData", params);
	}

}
