package com.qsyout.system.json.message;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qsyout.sdk.annotation.Api;
import com.qsyout.sdk.annotation.JsonResult;
import com.qsyout.sdk.declare.BaseService;
import com.qsyout.sdk.util.SubjectUtil;
import com.qsyout.system.consts.Credentials;
import com.qsyout.system.provider.MyMessageResolverService;

@Service
public class DeleteMyMessageDataService extends BaseService {
	
	@Autowired
	MyMessageResolverService myMessageResolver;

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	@Api(desc="系统管理->消息管理->删除",permission="system:message:delete")
	@JsonResult(info=true)
	public Object execute(Map params) {
		params.put("deleteId", SubjectUtil.getSession(Credentials.USER_ID));
		
		ibatisDao.update("SYS_MESSAGE.delete", params);
		myMessageResolver.update((String) params.get("key"));
		return null;
	}

}
