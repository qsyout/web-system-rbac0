package com.qsyout.system.json.message;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qsyout.sdk.annotation.Api;
import com.qsyout.sdk.annotation.JsonResult;
import com.qsyout.sdk.declare.BaseService;
import com.qsyout.sdk.ex.BusinessException;
import com.qsyout.sdk.util.SubjectUtil;
import com.qsyout.system.consts.Credentials;
import com.qsyout.system.provider.MyMessageResolverService;

@Service
public class UpdateMyMessageDataService extends BaseService {
	
	@Autowired
	MyMessageResolverService myMessageResolver;

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	@Api(desc="系统管理->消息管理->编辑",permission="system:message:update")
	@JsonResult(info=true)
	public Object execute(Map params) {
		params.put("updateId", SubjectUtil.getSession(Credentials.USER_ID));
		
		if((int)ibatisDao.load("SYS_MESSAGE.existKey", params) > 0){
			throw new BusinessException("UpdateMyMessageDataService.01", "存在相同key值");
		}
		
		if(params.get("id") == null){
			ibatisDao.update("SYS_MESSAGE.insert", params);
		}else{
			ibatisDao.update("SYS_MESSAGE.update", params);
		}
		
		myMessageResolver.update((String) params.get("key"));
		return null;
	}

}
