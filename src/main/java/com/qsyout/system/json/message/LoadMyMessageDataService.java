package com.qsyout.system.json.message;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.qsyout.sdk.annotation.Api;
import com.qsyout.sdk.declare.BaseService;

@Service
public class LoadMyMessageDataService extends BaseService {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	@Api(desc="系统管理->消息管理",record=false,permission="system:message")
	public Object execute(Map params) {
		long pageNumber = (long) params.get("pageNumber");
		long pageSize = (long) params.get("pageSize");
		
		params.put("start", (pageNumber - 1)*pageSize);
		
		Map result = new HashMap(2);
		result.put("total", ibatisDao.load("SYS_MESSAGE.pageQueryCount", params));
		result.put("list", ibatisDao.queryForList("SYS_MESSAGE.pageQuery", params));
		
		return result;
	}

}
