package com.qsyout.system.json.user;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.qsyout.sdk.annotation.Api;
import com.qsyout.sdk.declare.BaseService;
import com.qsyout.sdk.util.SubjectUtil;
import com.qsyout.system.consts.Credentials;

@Service
public class LogoutUpdateService extends BaseService {

	@SuppressWarnings("rawtypes")
	@Override
	@Api(desc="注销",open=true)
	public Object execute(Map params) {
		SubjectUtil.logout();
		SubjectUtil.removeSession(Credentials.AUTH_PERMISSIONS);
		SubjectUtil.removeSession(Credentials.HEADER_IMG);
		SubjectUtil.removeSession(Credentials.LAST_LOGIN_DATE);
		SubjectUtil.removeSession(Credentials.ROLE_IDS);
		SubjectUtil.removeSession(Credentials.USER_ID);
		SubjectUtil.removeSession(Credentials.ORG_CODE);
		SubjectUtil.removeSession(Credentials.USER_NAME);
		SubjectUtil.removeSession(Credentials.NICK_NAME);
		SubjectUtil.removeSession(Credentials.ORG_NAME);
		SubjectUtil.removeSession(Credentials.ROLE_NAMES);
		return null;
	}

}
