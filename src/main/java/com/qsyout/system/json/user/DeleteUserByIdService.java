package com.qsyout.system.json.user;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.qsyout.sdk.annotation.Api;
import com.qsyout.sdk.annotation.JsonResult;
import com.qsyout.sdk.declare.BaseService;
import com.qsyout.sdk.util.SubjectUtil;
import com.qsyout.system.consts.Credentials;

@Service
public class DeleteUserByIdService extends BaseService {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	@JsonResult(info=true)
	@Api(desc="系统管理->用户管理->删除用户",permission="system:user:delete")
	public Object execute(Map params) {
		params.put("updateId", SubjectUtil.getSession(Credentials.USER_ID));
		ibatisDao.update("SYS_USER.deleteUserById", params);
		return null;
	}

}
