package com.qsyout.system.json.user;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.qsyout.sdk.annotation.Api;
import com.qsyout.sdk.declare.BaseService;
import com.qsyout.sdk.util.SubjectUtil;
import com.qsyout.system.consts.Credentials;

@Service
public class LoadUserDataService extends BaseService {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	@Api(desc="系统管理->用户管理",permission="system:user",record=false)
	public Object execute(Map params) {
		long pageNumber = (long) params.get("pageNumber");
		long pageSize = (long) params.get("pageSize");
		
		params.put("start", (pageNumber - 1)*pageSize);
		params.put("id", SubjectUtil.getSession(Credentials.USER_ID));
		params.put("code", SubjectUtil.getSession(Credentials.ORG_CODE));
		
		Map result = new HashMap(2);
		result.put("total", ibatisDao.load("SYS_USER.pageQueryCount", params));
		result.put("list", ibatisDao.queryForList("SYS_USER.pageQuery", params));
		
		return result;
	}

}
