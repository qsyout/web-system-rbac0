package com.qsyout.system.json.user;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.qsyout.sdk.annotation.Api;
import com.qsyout.sdk.annotation.JsonResult;
import com.qsyout.sdk.declare.BaseService;
import com.qsyout.sdk.ex.BusinessException;
import com.qsyout.sdk.util.EncryptUtil;
import com.qsyout.sdk.util.SubjectUtil;
import com.qsyout.system.consts.Credentials;

@Service
public class UserLoginService extends BaseService {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	@Api(desc="账户登录",open=true)
	@JsonResult(info=true,message="登录成功")
	public Object execute(Map params) {
		//验证码验证
		String sessionCode = (String) SubjectUtil.getSession(Credentials.LOGIN_VERIFY_CODE);
		SubjectUtil.removeSession(Credentials.LOGIN_VERIFY_CODE);
		if(!((String)params.get("loginCode")).equals(sessionCode)){
			throw new BusinessException("UserLoginService.01", "验证码错误");
		}
		
		Map user = ibatisDao.load("SYS_USER.getByLoginName", params);
		
		if(user == null){
			throw new BusinessException("UserLoginService.02", "登录名或者密码错误");
		}
		
		if(!((String)user.get("loginPwd")).equals(EncryptUtil.encrypt((String)params.get("loginPwd"), (String)user.get("salt")))){
			throw new BusinessException("UserLoginService.02", "登录名或者密码错误");
		}
		
		if((int)user.get("enable") == 2){
			throw new BusinessException("UserLoginService.03", "账户已冻结，请联系管理员处理");
		}
		
		SubjectUtil.login();
		SubjectUtil.setSession(Credentials.HEADER_IMG, user.get("headImg"));
		SubjectUtil.setSession(Credentials.LAST_LOGIN_DATE, user.get(Credentials.LAST_LOGIN_DATE));
		SubjectUtil.setSession(Credentials.ROLE_IDS, user.get(Credentials.ROLE_IDS));
		SubjectUtil.setSession(Credentials.USER_ID, user.get("id"));
		SubjectUtil.setSession(Credentials.ORG_CODE, user.get(Credentials.ORG_CODE));
		SubjectUtil.setSession(Credentials.USER_NAME, user.get(Credentials.USER_NAME));
		SubjectUtil.setSession(Credentials.NICK_NAME, user.get(Credentials.NICK_NAME));
		SubjectUtil.setSession(Credentials.ORG_NAME, user.get(Credentials.ORG_NAME));
		SubjectUtil.setSession(Credentials.ROLE_NAMES, user.get(Credentials.ROLE_NAMES));
		SubjectUtil.setSession(Credentials.IS_ORIGINAL_PWD, Integer.parseInt(user.get("originalPwd").toString()) == 1);
		
		if (Long.parseLong(user.get("id") + "") != 1) {
			params.put("userId", user.get("id"));
			SubjectUtil.setSession(Credentials.AUTH_PERMISSIONS,
					ibatisDao.queryForList("SYS_MENU_PERMISSION.getPermissionsByUserId", params));
		}
		
		ibatisDao.update("SYS_USER.updateLastLoginDate", user);
		return null;
	}

}
