package com.qsyout.system.json.user;

import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.qsyout.sdk.annotation.Api;
import com.qsyout.sdk.annotation.JsonResult;
import com.qsyout.sdk.declare.BaseService;
import com.qsyout.sdk.ex.BusinessException;
import com.qsyout.sdk.util.EncryptUtil;
import com.qsyout.sdk.util.StringUtil;
import com.qsyout.sdk.util.SubjectUtil;
import com.qsyout.system.consts.Credentials;

@Service
public class InsertLoginUserService extends BaseService {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	@Api(desc="系统管理->用户管理->新增用户",permission="system:user:insert")
	@Transactional
	@JsonResult(info=true)
	public Object execute(Map params) {
		//验证登录账号的唯一性
		if((int)ibatisDao.load("SYS_USER.existLoginName", params) > 0){
			throw new BusinessException("InsertLoginUserService.01", "账户已存在");
		}
		String salt = StringUtil.uuid();
		params.put("salt", salt);
		params.put("loginPwd", EncryptUtil.encrypt((String) params.get("loginPwd"), salt));
		params.put("nickName", params.get("userName"));
		
		params.put("createId", SubjectUtil.getSession(Credentials.USER_ID));
		
		ibatisDao.update("SYS_USER.insert", params);
		params.put("userId", params.get("id"));
		ibatisDao.update("SYS_USER_ROLE.insert", params);
		return null;
	}

}
