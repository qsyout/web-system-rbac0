package com.qsyout.system.json.user;

import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.qsyout.sdk.annotation.Api;
import com.qsyout.sdk.annotation.JsonResult;
import com.qsyout.sdk.declare.BaseService;
import com.qsyout.sdk.util.SubjectUtil;
import com.qsyout.system.consts.Credentials;

@Service
public class UpdateUserInfoService extends BaseService {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	@Api(desc="系统管理->用户管理->编辑用户",permission="system:user:update")
	@Transactional
	@JsonResult(info=true)
	public Object execute(Map params) {
		params.put("updateId", SubjectUtil.getSession(Credentials.USER_ID));
		
		ibatisDao.update("SYS_USER.update", params);
		params.put("userId", params.get("id"));
		ibatisDao.update("SYS_USER_ROLE.deleteByUserId", params);
		ibatisDao.update("SYS_USER_ROLE.insert", params);
		return null;
	}

}
