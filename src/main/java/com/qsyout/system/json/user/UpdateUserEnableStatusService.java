package com.qsyout.system.json.user;

import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.qsyout.sdk.annotation.Api;
import com.qsyout.sdk.annotation.JsonResult;
import com.qsyout.sdk.declare.BaseService;
import com.qsyout.sdk.ex.ParamsValidateException;
import com.qsyout.sdk.util.SubjectUtil;
import com.qsyout.system.consts.Credentials;

@Service
public class UpdateUserEnableStatusService extends BaseService {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	@Api(desc="系统管理->用户管理->启用/冻结用户",permission="system:user:enable")
	@Transactional
	@JsonResult(info=true)
	public Object execute(Map params) {
		if(Long.parseLong(params.get("enable").toString()) == 2&&params.get("reason") == null){
			throw new ParamsValidateException("冻结原因不能为空");
		}
		params.put("updateId", SubjectUtil.getSession(Credentials.USER_ID));
		
		ibatisDao.update("SYS_USER.updateEnableStatus", params);
		return null;
	}

}
