package com.qsyout.system.json.user;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.qsyout.sdk.annotation.Api;
import com.qsyout.sdk.annotation.JsonResult;
import com.qsyout.sdk.declare.BaseService;
import com.qsyout.sdk.util.EncryptUtil;
import com.qsyout.sdk.util.SubjectUtil;
import com.qsyout.system.consts.Credentials;

@Service
public class ResetUserPwdService extends BaseService {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	@Api(desc="系统管理->用户管理->重置密码",permission="system:user:reset")
	@JsonResult(info=true)
	public Object execute(Map params) {
		Map user = ibatisDao.load("SYS_USER.getById", params);
		
		params.put("loginPwd", EncryptUtil.encrypt((String) user.get("loginName"), (String) user.get("salt")));
		params.put("updateId", SubjectUtil.getSession(Credentials.USER_ID));
		
		ibatisDao.update("SYS_USER.resetUserPwd", params);
		return null;
	}

}
