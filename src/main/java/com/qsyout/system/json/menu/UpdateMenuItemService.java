package com.qsyout.system.json.menu;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.qsyout.sdk.annotation.Api;
import com.qsyout.sdk.annotation.JsonResult;
import com.qsyout.sdk.declare.BaseService;
import com.qsyout.sdk.ex.BusinessException;

@Service
public class UpdateMenuItemService extends BaseService {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	@Api(desc="系统管理->菜单管理->更新菜单",permission="menu:update")
	@JsonResult(info=true)
	public Object execute(Map params) {
		if("name".equals(params.get("field"))&&(int)ibatisDao.load("SYS_MENU.existSameName", params) > 0){
			//验证同级别是否有相同的菜单名
			throw new BusinessException("UpdateMenuItemService.01", "当前级别存在相同的菜单名称!");
		}
		if("permissions".equals(params.get("field"))&&params.get("value") != null&&!"".equals(params.get("value"))){
			params.put("menuId", params.get("id"));
			params.put("list", ((String)params.get("value")).split(","));
			ibatisDao.update("SYS_MENU_PERMISSION.deleteByMenuId", params);
			ibatisDao.update("SYS_MENU_PERMISSION.updateMenuPermission", params);
		}else
			ibatisDao.update("SYS_MENU.updateMenuItem", params);
		
		return null;
	}

}
