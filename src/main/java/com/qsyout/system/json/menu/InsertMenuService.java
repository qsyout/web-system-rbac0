package com.qsyout.system.json.menu;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.qsyout.sdk.annotation.Api;
import com.qsyout.sdk.annotation.JsonResult;
import com.qsyout.sdk.declare.BaseService;
import com.qsyout.sdk.ex.BusinessException;

/**  
 * @Description:  新增菜单
 * @author: dangsj     
 * @date: 2020-08-08 11:08 
 * @Copyright: All rights Reserved, Designed By qsyout.com
 */
@Service
public class InsertMenuService extends BaseService {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	@Api(desc="系统管理->菜单管理->新增菜单 ",permission="menu:insert")
	@JsonResult(info=true)
	public Object execute(Map params) {
		//同级别组织名称不能一样
		if((int)ibatisDao.load("SYS_MENU.existSameName", params) > 0){
			throw new BusinessException("InsertMenuService.01", "当前级别存在相同的菜单名称!");
		}
		params.put("sort", ibatisDao.load("SYS_MENU.loadNextSortByPid", params));
		ibatisDao.update("SYS_MENU.insert", params);
		return null;
	}

}
