package com.qsyout.system.json.menu;

import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.qsyout.sdk.annotation.Api;
import com.qsyout.sdk.annotation.JsonResult;
import com.qsyout.sdk.declare.BaseService;
import com.qsyout.sdk.ex.BusinessException;

/**  
 * @Description:删除菜单
 * @author: dangsj     
 * @date: 2020-08-08 11:12 
 * @Copyright: All rights Reserved, Designed By qsyout.com
 */
@Service
public class DelMenuService extends BaseService {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	@Api(desc="系统管理->菜单管理->删除菜单 ",permission="menu:delete")
	@Transactional
	@JsonResult(info=true)
	public Object execute(Map params) {
		//判断当前组织下是否有子级组织机构
		if((int)ibatisDao.load("SYS_MENU.hasChildren", params) > 0){
			throw new BusinessException("DelMenuService.01", "存在子级菜单，不能删除!");
		}
		
		ibatisDao.update("SYS_MENU.delete", params);
		//删除关联角色绑定
		params.put("menuId", params.get("id"));
		ibatisDao.update("SYS_ROLE_MENU.deleteByMenuId", params);
		return null;
	}

}
