package com.qsyout.system.json.menu;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.qsyout.sdk.annotation.Api;
import com.qsyout.sdk.declare.BaseService;

@Service
public class LoadMenuDataService extends BaseService {

	@SuppressWarnings("rawtypes")
	@Override
	@Api(desc="系统管理->菜单管理->加载菜单数据",record=false,permission="system:menu")
	public Object execute(Map params) {
		return ibatisDao.queryForList("SYS_MENU.loadMenuData", params);
	}

}
