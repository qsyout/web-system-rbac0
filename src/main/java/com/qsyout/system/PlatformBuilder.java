package com.qsyout.system;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.core.io.ClassPathResource;

/**
 * 数据库初始化
 * @Description:仅支持mysql数据库   
 * @author: dangsj     
 * @date: 2020-08-05 17:40 
 * @Copyright: All rights Reserved, Designed By qsyout.com
 */
public class PlatformBuilder {
	
	private Connection connection;
	
	private PlatformBuilder(Connection connection) {
		this.connection = connection;
	}

	/**
	 * @param host 数据库服务器ip
	 * @param port 数据库端口
	 * @param dbName
	 * @param userName
	 * @param userPwd
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static PlatformBuilder build(String host,int port,String dbName,String userName,String userPwd) throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		return new PlatformBuilder(DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + dbName + "?characterEncoding=UTF8&useSSL=false", userName, userPwd));
	}
	
	public void execute() throws SQLException {
		Statement st = connection.createStatement();
		
		try {
			ClassPathResource resource = new ClassPathResource("init.sql");
			BufferedReader reader = new BufferedReader(new InputStreamReader(resource.getInputStream()));
			StringBuilder sql = null;
			String line;
			while ((line = reader.readLine()) != null) {
				if (sql == null) {
					sql = new StringBuilder();
				}
				if (!"".equals(line)) {
					sql.append(line);
				}
				if (line.endsWith(";")) {
					st.executeUpdate(sql.toString());
					sql = null;
				}
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		st.close();
		connection.close();
	}
}
