package com.qsyout.system.action;

import java.awt.image.BufferedImage;
import java.util.Properties;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.code.kaptcha.impl.DefaultKaptcha;
import com.google.code.kaptcha.util.Config;
import com.qsyout.system.consts.Credentials;

@Controller
@RequestMapping("/sys")
public class LoginVerifyCodeController {

	private DefaultKaptcha captcha = new DefaultKaptcha();

	public LoginVerifyCodeController() {
		Properties properties = new Properties();
		properties.setProperty("kaptcha.image.width", "120");
		properties.setProperty("kaptcha.image.height", "38");
		properties.setProperty("kaptcha.textproducer.font.color", "red");
		properties.setProperty("kaptcha.textproducer.font.size", "35");
		properties.setProperty("kaptcha.textproducer.char.length", "4");
		Config config = new Config(properties);
		captcha.setConfig(config);
	}

	@GetMapping("/getKaptchaVerify.do")
	public void getKaptchaVerify(HttpServletRequest request, HttpServletResponse response) throws Exception {
		response.setDateHeader("Expires", 0);
		response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
		response.addHeader("Cache-Control", "post-check=0, pre-check=0");
		response.setHeader("Pragma", "no-cache");
		response.setContentType("image/jpeg");

		String capText = captcha.createText();
		
		request.getSession().setAttribute(Credentials.LOGIN_VERIFY_CODE, capText);

		BufferedImage bi = captcha.createImage(capText);
		ServletOutputStream out = response.getOutputStream();

		ImageIO.write(bi, "jpg", out);
		try {
			out.flush();
		} finally {
			out.close();
		}
	}
}
