package com.qsyout.system.action;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.qsyout.sdk.annotation.Api;
import com.qsyout.sdk.util.SubjectUtil;
import com.qsyout.system.consts.Credentials;

@Controller
public class SystemViewController implements ErrorController  {

	@RequestMapping(value={"","/","/index","/index.htm","/index.html"})
	@Api(desc="首页",record=false,open=true)
	public String index(){
		return "/index.html";
	}
	
	@RequestMapping("/error")
	public String error(){
		return "/404.html";
	}
	
	@RequestMapping("/system/forget.html")
	public String forgetPwd(){
		return "/system/forget.html";
	}
	
	@RequestMapping(value={"system","/system","/system/","/system/index","/system/index.htm","/system/index.html"})
	@Api(desc="系统管理->首页",record=false)
	public String system(){
		Integer userId = (Integer) SubjectUtil.getSession(Credentials.USER_ID);
		if(userId == 1){
			return "/system/admin.html";//系统管理员界面
		}
		return "/system/welcome.html";
	}
	
	
	@RequestMapping("/system/organization.html")
	@Api(desc="系统管理->组织管理",record=false,permission="system:organization")
	public String organization(HttpServletRequest request){
		return "/system/organization.html";
	}
	
	@RequestMapping("/system/menu.html")
	@Api(desc="系统管理->菜单管理",record=false,permission="system:menu")
	public String menu(HttpServletRequest request){
		return "/system/menu.html";
	}
	
	@RequestMapping("/system/role.html")
	@Api(desc="系统管理->角色管理",record=false,permission="system:role")
	public String role(HttpServletRequest request){
		return "/system/role.html";
	}
	
	@RequestMapping("/system/roleMenu.html")
	@Api(desc="系统管理->角色管理->权限设置",record=false,permission="system:role:roleMenu")
	public String roleMenu(HttpServletRequest request){
		return "/system/roleMenu.html";
	}
	
	@RequestMapping("/system/user.html")
	@Api(desc="系统管理->用户管理",record=false,permission="system:user")
	public String user(HttpServletRequest request){
		return "/system/user.html";
	}
	
	@RequestMapping("/system/dictionary.html")
	@Api(desc="系统管理->数据字典",record=false,permission="system:dictionary")
	public String dictionary(HttpServletRequest request){
		return "/system/dictionary.html";
	}
	
	@RequestMapping("/system/message.html")
	@Api(desc="系统管理->消息管理",record=false,permission="system:message")
	public String message(HttpServletRequest request){
		return "/system/message.html";
	}
	
	@RequestMapping("/system/log.html")
	@Api(desc="系统管理->日志管理",record=false,permission="system:log")
	public String log(HttpServletRequest request){
		return "/system/log.html";
	}
	
	@RequestMapping("/system/personal.html")
	@Api(desc="个人中心->个人资料",record=false)
	public String personal(HttpServletRequest request){
		return "/system/personal.html";
	}
	
	@RequestMapping("/system/safe.html")
	@Api(desc="个人中心->安全中心",record=false)
	public String safe(HttpServletRequest request){
		return "/system/safe.html";
	}
	
	@RequestMapping("/login.html")
	@Api(desc="登录页面",record=false)
	public String login(HttpServletRequest request){
		return "/login.html";
	}

	@Override
	public String getErrorPath() {
		return "/error";
	}
	
	@RequestMapping("/system/database.html")
	@Api(desc="系统管理->数据库",record=false,permission="system:database")
	public String database(HttpServletRequest request){
		return "/system/database.html";
	}
	
}
