package com.qsyout.system.action;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.qsyout.sdk.annotation.Api;
import com.qsyout.sdk.annotation.JsonResult;
import com.qsyout.sdk.declare.FileService;
import com.qsyout.sdk.util.StringUtil;

@Controller
@RequestMapping("/sys")
public class SystemFileUploadController {
	
	@Autowired
	FileService fileService;
	
	private final String prefix = "/file";
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

	@RequestMapping("/user/headImg.do")
	@Api(desc="个人中心->头像上传",record=false)
	@JsonResult
	public String headImg(MultipartHttpServletRequest request) throws IOException{
		MultipartFile file = request.getFile("file");
		String original = file.getOriginalFilename();
		String ext = original.substring(original.lastIndexOf("."));

		String path = "/" + sdf.format(new Date()) + "/user" + prefix + "/";
		String fileName = StringUtil.uuid() + ext;

		fileService.saveFile(path, fileName, file.getInputStream());
		return path + fileName;
	}
}
