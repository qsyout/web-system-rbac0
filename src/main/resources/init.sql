SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS `sys_dictionary`;
CREATE TABLE `sys_dictionary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `groupName` varchar(50) NOT NULL COMMENT '字典组名称',
  `dictionaryName` varchar(50) NOT NULL COMMENT '字典名称',
  `dictionaryValue` int(11) NOT NULL COMMENT '字典值',
  `dictionaryDesc` varchar(255) DEFAULT NULL COMMENT '字典描述',
  `createId` int(11) NOT NULL,
  `createDate` datetime NOT NULL,
  `updateId` int(11) NOT NULL,
  `updateDate` datetime NOT NULL,
  `isDelete` tinyint(4) NOT NULL,
  `deleteId` int(11) DEFAULT NULL,
  `deleteDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_pid` (`pid`),
  KEY `idx_group_name` (`groupName`) USING HASH,
  KEY `idx_dictionary_name` (`dictionaryName`) USING HASH,
  KEY `idx_create_id` (`createId`),
  KEY `idx_update_id` (`updateId`),
  KEY `idx_update_date` (`updateDate`),
  KEY `idx_is_delete` (`isDelete`),
  KEY `idx_group_value` (`groupName`,`dictionaryValue`),
  KEY `idx_group_value_delete` (`groupName`,`dictionaryValue`,`isDelete`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COMMENT='数据字典表';

DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(150) NOT NULL,
  `desc` varchar(100) DEFAULT NULL,
  `ex` varchar(100) DEFAULT NULL,
  `open` tinyint(4) NOT NULL,
  `userId` int(11) DEFAULT NULL,
  `createDate` datetime NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1200' COMMENT '请求状态',
  `use` int(11) DEFAULT NULL COMMENT '耗时',
  `sn` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`userId`),
  KEY `idx_url` (`url`),
  KEY `idx_create_date` (`createDate`),
  KEY `idx_status` (`status`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COMMENT='日志记录表';


DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL COMMENT '菜单名称',
  `url` varchar(50) DEFAULT NULL COMMENT '视图路径',
  `icon` varchar(50) DEFAULT NULL COMMENT '视图 图标',
  `view` tinyint(4) NOT NULL COMMENT '是否视图权限，1是2否',
  `isSystem` tinyint(4) NOT NULL DEFAULT '2' COMMENT '系统预留项，1是2否',
  `pid` int(11) NOT NULL COMMENT '上级菜单',
  `code` varchar(50) NOT NULL COMMENT '仅作为查询时的筛选',
  `intCode` int(4) NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序，同级别越小越靠前',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uni_code` (`code`) USING HASH,
  KEY `idx_pid` (`pid`),
  KEY `idx_sort` (`sort`),
  KEY `idx_view` (`view`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COMMENT='菜单表';

DROP TABLE IF EXISTS `sys_menu_permission`;
CREATE TABLE `sys_menu_permission` (
  `menuId` int(11) NOT NULL COMMENT '对应sys_menu表id',
  `permission` varchar(50) NOT NULL COMMENT '权限标识',
  UNIQUE KEY `uni_menu_permission` (`menuId`,`permission`),
  KEY `idx_permission` (`permission`) USING HASH,
  KEY `idx_menu_id` (`menuId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='菜单权限标识表';

DROP TABLE IF EXISTS `sys_message`;
CREATE TABLE `sys_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(50) NOT NULL,
  `value` varchar(100) NOT NULL,
  `createId` int(11) NOT NULL,
  `createDate` datetime NOT NULL,
  `updateId` int(11) NOT NULL,
  `updateDate` datetime NOT NULL,
  `isDelete` tinyint(4) NOT NULL,
  `deleteId` int(11) DEFAULT NULL,
  `deleteDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_key` (`key`) USING HASH,
  KEY `idx_value` (`value`) USING HASH,
  KEY `idx_create_id` (`createId`),
  KEY `idx_update_id` (`updateId`),
  KEY `idx_update_date` (`updateDate`),
  KEY `idx_is_delete` (`isDelete`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COMMENT='异常消息自定义返回提示';

DROP TABLE IF EXISTS `sys_organization`;
CREATE TABLE `sys_organization` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL COMMENT '组织机构名称',
  `code` varchar(100) NOT NULL COMMENT '组织机构编码',
  `pid` int(11) NOT NULL,
  `createId` int(11) NOT NULL,
  `createDate` datetime NOT NULL,
  `updateId` int(11) NOT NULL,
  `updateDate` datetime NOT NULL,
  `isSystem` tinyint(4) NOT NULL COMMENT '是否系统预留,1是2否',
  `isDelete` tinyint(4) NOT NULL COMMENT '是否删除，1是2否',
  `deleteId` int(11) DEFAULT NULL,
  `deleteDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_code` (`code`),
  KEY `idx_is_system` (`isSystem`),
  KEY `idx_is_delete` (`isDelete`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;

CREATE TABLE `sys_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `isSystem` tinyint(4) NOT NULL COMMENT '是否系统预留，1是，2否',
  `createId` int(11) NOT NULL COMMENT '创建人id',
  `createDate` datetime NOT NULL COMMENT '创建时间',
  `updateId` int(11) NOT NULL,
  `updateDate` datetime NOT NULL,
  `isDelete` tinyint(4) NOT NULL COMMENT '是否删除，1是，2否',
  `deleteId` int(11) DEFAULT NULL,
  `deleteDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `roleId` int(11) NOT NULL COMMENT '角色id，对应sys_role表id',
  `menuId` int(11) NOT NULL COMMENT '菜单id，对应sys_menu表id',
  PRIMARY KEY (`roleId`,`menuId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `sys_safe_answers`;
CREATE TABLE `sys_safe_answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL COMMENT '用户id',
  `questionId` int(11) NOT NULL COMMENT '安全问题id,对应数据字典safe_questions的id',
  `answer` varchar(255) NOT NULL,
  `createDate` datetime NOT NULL,
  `updateDate` datetime NOT NULL,
  `isDelete` tinyint(4) NOT NULL COMMENT '是否删除',
  `deleteDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`userId`),
  KEY `idx_question_id` (`questionId`),
  KEY `idx_is_delete` (`isDelete`),
  KEY `idx_update_date` (`updateDate`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COMMENT='每个用户仅限2个密保问题';

DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `loginName` varchar(20) NOT NULL,
  `loginPwd` varchar(32) NOT NULL,
  `salt` varchar(32) NOT NULL,
  `originalPwd` tinyint(4) NOT NULL DEFAULT '1' COMMENT '是否原始密码，1是2否',
  `userName` varchar(20) NOT NULL COMMENT '用户名',
  `nickName` varchar(30) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL COMMENT '邮件，超级管理员账户，须填写邮件以便系统发送警告信息',
  `orgId` int(11) DEFAULT NULL COMMENT '所属组织机构id,rbac0权限模式时这样设计,其他模式配置关联表',
  `headImg` varchar(100) DEFAULT NULL COMMENT '/images/default-header.png',
  `enable` tinyint(4) NOT NULL COMMENT '是否启用,1是，2否',
  `lastLoginDate` datetime DEFAULT NULL COMMENT '上一次登录时间',
  `createId` int(11) NOT NULL,
  `createDate` datetime NOT NULL,
  `updateId` int(11) NOT NULL,
  `updateDate` datetime NOT NULL,
  `isDelete` tinyint(4) NOT NULL COMMENT '是否删除，1是2否',
  `deleteId` int(11) DEFAULT NULL,
  `deleteDate` datetime DEFAULT NULL,
  `reason` varchar(255) DEFAULT NULL COMMENT '禁用或删除时的原因',
  PRIMARY KEY (`id`),
  KEY `idx_login_name` (`loginName`),
  KEY `idx_enable` (`enable`),
  KEY `idx_is_delete` (`isDelete`),
  KEY `idx_org_id` (`orgId`),
  KEY `idx_user_name` (`userName`),
  KEY `idx_create_id` (`createId`),
  KEY `idx_update_id` (`updateId`),
  KEY `idx_update_date` (`updateDate`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `userId` int(11) NOT NULL COMMENT '用户id，对应sys_user表id',
  `roleId` int(11) NOT NULL COMMENT '角色id，对应sys_role表id',
  PRIMARY KEY (`userId`,`roleId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- 初始数据，系统预留数据
INSERT INTO `sys_user` (`id`, `loginName`, `loginPwd`, `salt`, `originalPwd`, `userName`, `nickName`, `mobile`, `email`, `orgId`, `headImg`, `enable`, `lastLoginDate`, `createId`, `createDate`, `updateId`, `updateDate`, `isDelete`) VALUES ('1', 'admin', '41d3e1a867e90d8b21d914343b9594e7', '9fb7a680208b4d98a0d8e43690858a58', '1', '系统管理员', '超级管理员', '', '', '1', '/images/default-header.png', '1', NULL, 1, now(), '1', now(), '2');
INSERT INTO `sys_role` (`id`, `name`, `isSystem`, `createId`, `createDate`, `updateId`, `updateDate`, `isDelete`) VALUES ('1', '系统管理员', '1', '1', now(), '0', now(), '2');
INSERT INTO `sys_user_role` (`userId`, `roleId`) VALUES ('1', '1');
INSERT INTO `sys_organization` (`id`, `name`, `code`, `pid`, `createId`, `createDate`, `updateId`, `updateDate`, `isSystem`, `isDelete`) VALUES ('1', '顶级机构', '0001', '0', '1', now(), '1', now(), '1', '2');
INSERT INTO `sys_dictionary` (`id`, `pid`, `groupName`, `dictionaryName`, `dictionaryValue`, `dictionaryDesc`, `createId`, `createDate`, `updateId`, `updateDate`, `isDelete`) VALUES ('1', '0', 'mail_templates', '重置密码,邮箱模板', '1', '尊敬的用户您好，您的账号[%s]正在进行密码重置，重置验证码为：%s。<br/>此邮件为系统邮件,请勿回复!<br/><br/>如有疑问请登录：%s。', '1', now(), '1', now(), '2');
INSERT INTO `sys_dictionary` (`id`, `pid`, `groupName`, `dictionaryName`, `dictionaryValue`, `dictionaryDesc`, `createId`, `createDate`, `updateId`, `updateDate`, `isDelete`) VALUES ('2', '0', 'safe_questions', '你初恋的名字叫什么？', '1', '', '1', now(), '1', now(), '2');
INSERT INTO `sys_menu` (`id`, `name`, `url`, `icon`, `view`, `isSystem`, `pid`, `code`, `intCode`, `sort`) VALUES ('1', '系统设置', NULL, 'el-icon-setting', '1', '1', '0', '0001', '1', '1');
INSERT INTO `sys_menu` (`id`, `name`, `url`, `icon`, `view`, `isSystem`, `pid`, `code`, `intCode`, `sort`) VALUES ('2', '组织管理', '${ctxPath}/system/organization.html', 'el-icon-office-building', '1', '1', '1', '00010001', '1', '1');
INSERT INTO `sys_menu` (`id`, `name`, `url`, `icon`, `view`, `isSystem`, `pid`, `code`, `intCode`, `sort`) VALUES ('3', '用户管理', '${ctxPath}/system/user.html', 'el-icon-yonghuguanli', '1', '1', '1', '00010002', '2', '2');
INSERT INTO `sys_menu` (`id`, `name`, `url`, `icon`, `view`, `isSystem`, `pid`, `code`, `intCode`, `sort`) VALUES ('4', '数据字典', '${ctxPath}/system/dictionary.html', 'el-icon-reading', '1', '1', '1', '00010003', '3', '3');
INSERT INTO `sys_menu` (`id`, `name`, `url`, `icon`, `view`, `isSystem`, `pid`, `code`, `intCode`, `sort`) VALUES ('5', '异常消息', '${ctxPath}/system/message.html', 'el-icon-message', '1', '1', '1', '00010004', '4', '4');
INSERT INTO `sys_menu` (`id`, `name`, `url`, `icon`, `view`, `isSystem`, `pid`, `code`, `intCode`, `sort`) VALUES ('6', '新增', NULL, NULL, '2', '1', '2', '000100010001', '1', '1');
INSERT INTO `sys_menu` (`id`, `name`, `url`, `icon`, `view`, `isSystem`, `pid`, `code`, `intCode`, `sort`) VALUES ('7', '编辑', NULL, NULL, '2', '1', '2', '000100010002', '2', '2');
INSERT INTO `sys_menu` (`id`, `name`, `url`, `icon`, `view`, `isSystem`, `pid`, `code`, `intCode`, `sort`) VALUES ('8', '删除', NULL, NULL, '2', '1', '2', '000100010003', '3', '3');
INSERT INTO `sys_menu` (`id`, `name`, `url`, `icon`, `view`, `isSystem`, `pid`, `code`, `intCode`, `sort`) VALUES ('9', '新增', NULL, NULL, '2', '1', '3', '000100020001', '1', '1');
INSERT INTO `sys_menu` (`id`, `name`, `url`, `icon`, `view`, `isSystem`, `pid`, `code`, `intCode`, `sort`) VALUES ('10', '编辑', NULL, NULL, '2', '1', '3', '000100020002', '2', '2');
INSERT INTO `sys_menu` (`id`, `name`, `url`, `icon`, `view`, `isSystem`, `pid`, `code`, `intCode`, `sort`) VALUES ('11', '启用/冻结', NULL, NULL, '2', '1', '3', '000100020003', '3', '3');
INSERT INTO `sys_menu` (`id`, `name`, `url`, `icon`, `view`, `isSystem`, `pid`, `code`, `intCode`, `sort`) VALUES ('12', '重置密码', NULL, NULL, '2', '1', '3', '000100020004', '4', '4');
INSERT INTO `sys_menu` (`id`, `name`, `url`, `icon`, `view`, `isSystem`, `pid`, `code`, `intCode`, `sort`) VALUES ('13', '删除', NULL, NULL, '2', '1', '3', '000100020005', '5', '5');
INSERT INTO `sys_menu` (`id`, `name`, `url`, `icon`, `view`, `isSystem`, `pid`, `code`, `intCode`, `sort`) VALUES ('14', '新增', NULL, NULL, '2', '1', '4', '000100030001', '1', '1');
INSERT INTO `sys_menu` (`id`, `name`, `url`, `icon`, `view`, `isSystem`, `pid`, `code`, `intCode`, `sort`) VALUES ('15', '编辑', NULL, NULL, '2', '1', '4', '000100030002', '2', '2');
INSERT INTO `sys_menu` (`id`, `name`, `url`, `icon`, `view`, `isSystem`, `pid`, `code`, `intCode`, `sort`) VALUES ('16', '删除', NULL, NULL, '2', '1', '4', '000100030003', '3', '3');
INSERT INTO `sys_menu` (`id`, `name`, `url`, `icon`, `view`, `isSystem`, `pid`, `code`, `intCode`, `sort`) VALUES ('18', '新增/编辑', NULL, NULL, '2', '1', '5', '000100040002', '2', '2');
INSERT INTO `sys_menu` (`id`, `name`, `url`, `icon`, `view`, `isSystem`, `pid`, `code`, `intCode`, `sort`) VALUES ('19', '删除', NULL, NULL, '2', '1', '5', '000100040003', '3', '3');
INSERT INTO `sys_menu_permission` (`menuId`, `permission`) VALUES ('1', 'system');
INSERT INTO `sys_menu_permission` (`menuId`, `permission`) VALUES ('2', 'system:organ');
INSERT INTO `sys_menu_permission` (`menuId`, `permission`) VALUES ('3', 'system:user');
INSERT INTO `sys_menu_permission` (`menuId`, `permission`) VALUES ('4', 'system:dictionary');
INSERT INTO `sys_menu_permission` (`menuId`, `permission`) VALUES ('5', 'system:message');
INSERT INTO `sys_menu_permission` (`menuId`, `permission`) VALUES ('6', 'organization:insert');
INSERT INTO `sys_menu_permission` (`menuId`, `permission`) VALUES ('7', 'organization:update');
INSERT INTO `sys_menu_permission` (`menuId`, `permission`) VALUES ('8', 'organization:delete');
INSERT INTO `sys_menu_permission` (`menuId`, `permission`) VALUES ('9', 'system:user:insert');
INSERT INTO `sys_menu_permission` (`menuId`, `permission`) VALUES ('10', 'system:user:update');
INSERT INTO `sys_menu_permission` (`menuId`, `permission`) VALUES ('11', 'system:user:enable');
INSERT INTO `sys_menu_permission` (`menuId`, `permission`) VALUES ('12', 'system:user:reset');
INSERT INTO `sys_menu_permission` (`menuId`, `permission`) VALUES ('13', 'system:user:delete');
INSERT INTO `sys_menu_permission` (`menuId`, `permission`) VALUES ('14', 'system:dictionary:insert');
INSERT INTO `sys_menu_permission` (`menuId`, `permission`) VALUES ('15', 'system:dictionary:update');
INSERT INTO `sys_menu_permission` (`menuId`, `permission`) VALUES ('16', 'system:dictionary:delete');
INSERT INTO `sys_menu_permission` (`menuId`, `permission`) VALUES ('18', 'system:message:update');
INSERT INTO `sys_menu_permission` (`menuId`, `permission`) VALUES ('19', 'system:message:delete');

