# web-system-rbac0

#### 介绍
基于最基础的权限[用户-角色-权限]控制构建成的系统管理模块，以便于使用时拿来即用

#### 软件架构
软件架构说明：  
	springboot 2.0.0.RELEASE
	web-sdk 1.0.0.RELEASE
	beetl 2.9.3
	mysql 5.7
	vue.js 2.6.x + element-ui@2.13.x

#### 安装教程

1.在pom.xml引入：
```
<dependency>
	<groupId>com.qsyout</groupId>
	<artifactId>web-system-rbac0</artifactId>
	<version>1.0.0.RELEASE</version>
</dependency>
```
2.在main方法中执行：
```
PlatformBuilder.build(...);
```
3.以springboot的启动方式启动

#### 使用说明

1.xml校验文件在resources的receives文件下定义
2.xml文件头定义如下：
```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE map PUBLIC "-//qsyout.com,receive map params validate tag library" "http://qsyout.gitee.io/validate-starter/validate.dtd" >
```
3.beetl模板须放置在resources/views下

#### 首页权限：system:index
1.首页对应左侧菜单：工作台
2.工作台分为2种类型：
	超级管理员页面：服务器监控[CPU、内存、硬盘],/system/admin.html
	其他角色类型：默认欢迎页，可覆盖欢迎页，在views的路径下重新定义/system/welcome.html即可，具体用法参考默认页
